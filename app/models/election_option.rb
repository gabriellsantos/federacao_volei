# == Schema Information
#
# Table name: election_options
#
#  id          :bigint           not null, primary key
#  descricao   :text
#  imagem      :string
#  nome        :string
#  numero      :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  election_id :bigint           not null
#
# Indexes
#
#  index_election_options_on_election_id  (election_id)
#
# Foreign Keys
#
#  fk_rails_...  (election_id => elections.id)
#
class ElectionOption < ApplicationRecord
  belongs_to :election
  mount_uploader :imagem, DocumentoUploader
end
