# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  admin                  :boolean
#  ativo                  :boolean
#  cep                    :string
#  cidade                 :string
#  cod_fed_estadual       :string
#  cod_fed_nacional       :string
#  cpf                    :string
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  endereco               :string
#  foto_cpf_frente        :string
#  foto_cpf_verso         :string
#  foto_rg_frente         :string
#  foto_rg_verso          :string
#  foto_rosto             :string
#  modalidade             :string
#  nascimento             :date
#  nome                   :string
#  nome_mae               :string
#  nome_pai               :string
#  pode_votar             :boolean
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  rg                     :string
#  sexo                   :string
#  telefone               :string
#  whatsapp               :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#
class User < ApplicationRecord
  extend Enumerize
  mount_uploader :foto_rosto, DocumentoUploader
  mount_uploader :foto_rg_verso, DocumentoUploader
  mount_uploader :foto_rg_frente, DocumentoUploader
  mount_uploader :foto_cpf_verso, DocumentoUploader
  mount_uploader :foto_cpf_frente, DocumentoUploader
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  enumerize :sexo, in: [:masculino, :feminino], predicates: true
  enumerize :modalidade, in: [:praia, :quadra], predicates: true

  has_many :member_votes

  before_create :set_ativo

  def set_ativo
    self.ativo = false
    self.admin = false
  end


  #Método que retorna o nome do usuário
  def to_s
    self.nome
  end

  #Métdo que retorna o stats do usuario
  def usuario_ativo?
    self.ativo
  end

  #Método que verifica se o usuário está ativo
  def active_for_authentication?
    super && usuario_ativo?
  end
end
