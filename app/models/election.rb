# == Schema Information
#
# Table name: elections
#
#  id         :bigint           not null, primary key
#  descricao  :text
#  fim        :datetime
#  inicio     :datetime
#  titulo     :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Election < ApplicationRecord
  has_many :election_options

  accepts_nested_attributes_for :election_options, reject_if: :all_blank, allow_destroy: true

  #Método que verifica se a eleição está ativa
  def ativa?
    self.fim > DateTime.now
  end
end
