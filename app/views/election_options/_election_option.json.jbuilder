json.extract! election_option, :id, :election_id, :nome, :descricao, :numero, :imagem, :created_at, :updated_at
json.url election_option_url(election_option, format: :json)
