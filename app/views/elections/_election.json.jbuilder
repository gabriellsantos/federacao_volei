json.extract! election, :id, :titulo, :descricao, :inicio, :fim, :created_at, :updated_at
json.url election_url(election, format: :json)
