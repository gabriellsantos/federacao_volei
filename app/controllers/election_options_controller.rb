class ElectionOptionsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_election_option, only: [:show, :edit, :update, :destroy]

  # GET /election_options
  def index
    @q = ElectionOption.all.ransack(params[:q])
    @election_options = @q.result.page(params[:page])
  end

  # GET /election_options/1
  def show
  end

  # GET /election_options/new
  def new
    @election_option = ElectionOption.new
  end

  # GET /election_options/1/edit
  def edit
  end

  # POST /election_options
  def create
    @election_option = ElectionOption.new(election_option_params)

    if @election_option.save
      redirect_to @election_option, notice: 'Election option foi criado com sucesso'
    else
      render :new
    end
  end

  # PATCH/PUT /election_options/1
  def update
    if @election_option.update(election_option_params)
      redirect_to @election_option, notice: 'Election option foi atualizado com sucesso.'
    else
      render :edit
    end
  end

  # DELETE /election_options/1
  def destroy
    @election_option.destroy
    redirect_to election_options_url, notice: 'Election option foi apagado com sucesso.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_election_option
      @election_option = ElectionOption.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def election_option_params
      params.require(:election_option).permit(:election_id, :nome, :descricao, :numero, :imagem)
    end
end
