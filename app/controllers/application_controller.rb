class ApplicationController < ActionController::Base

  before_action :configure_permitted_parameters, if: :devise_controller?

  rescue_from CanCan::AccessDenied do |exception|
    respond_to do |format|
      format.json { head :forbidden, content_type: 'text/html' }
      format.html { redirect_to root_path, notice: exception.message }
      format.js   { head :forbidden, content_type: 'text/html' }
    end
  end

  protected

  add_breadcrumb "Início", :root_path

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:cep, :cidade, :cpf, :email, :endereco, :foto_cpf_frente, :foto_cpf_verso, :foto_rg_frente, :foto_rg_verso, :foto_rosto, :modalidade, :nascimento, :nome, :nome_pai, :nome_mae, :rg, :sexo,:telefone, :whatsapp])
    devise_parameter_sanitizer.permit(:account_update, keys: [:cep, :cidade, :cpf, :email, :endereco, :foto_cpf_frente, :foto_cpf_verso, :foto_rg_frente, :foto_rg_verso, :foto_rosto, :modalidade, :nascimento, :nome, :nome_pai, :nome_mae, :rg, :sexo,:telefone, :whatsapp])
  end
end

