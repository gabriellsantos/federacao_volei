class ElectionsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource

  before_action :set_election, only: [:apuracao,:show, :edit, :update, :destroy]

  # GET /elections
  def index
    add_breadcrumb "Eleições", elections_path
    if current_user.admin
      @q = Election.all.ransack(params[:q])
      @elections = @q.result.page(params[:page])
      render "index"
    else
      @elections = Election.where("? between inicio and fim", DateTime.now)
      render "public_elections"
    end

  end

  def public_elections
    @elections = Election.where("? between inicio and fim", DateTime.now)
  end

  def apuracao
    add_breadcrumb "Eleições", elections_path
    add_breadcrumb @election.titulo, election_path(@election)
    add_breadcrumb "Apuração", apuracao_election_path(@election)
    @votos = Vote.where(election_option_id: @election.election_options.pluck(:id))

  end

  def recaptcha
    render json: {}
  end

  def votar
    @eleicao = Election.find(params[:eleicao])
    if !@eleicao.ativa?
      redirect_to root_path, notice: "Eleição encerrada!"
      return
    end
    if current_user.member_votes.where(election_id: params[:eleicao]).last.present?

      redirect_to @eleicao, notice: "Você já votou!"
      return
    else
      Vote.create(election_option_id: params[:opcao], chave: SecureRandom.uuid)
      MemberVote.create(chave: SecureRandom.uuid, user_id: current_user.id, election_id: params[:eleicao])
      redirect_to @eleicao, notice: "Voto computado com sucesso!"
      return
    end

  end

  # GET /elections/1
  def show
    add_breadcrumb "Eleições", elections_path
    add_breadcrumb @election.titulo, election_path(@election)
    @voto = current_user.member_votes.where(election_id: @election.id).last
  end

  # GET /elections/new
  def new
    add_breadcrumb "Eleições", elections_path
    add_breadcrumb "Cadastrar", new_election_path
    @election = Election.new
  end

  # GET /elections/1/edit
  def edit
    add_breadcrumb "Eleições", elections_path
    add_breadcrumb "Alterar", edit_election_path(@election)
    if !current_user.admin
      redirect_to root_path
    end
  end

  # POST /elections
  def create
    @election = Election.new(election_params)

    if @election.save
      redirect_to @election, notice: 'Election foi criado com sucesso'
    else
      render :new
    end
  end

  # PATCH/PUT /elections/1
  def update
    if @election.update(election_params)
      redirect_to @election, notice: 'Election foi atualizado com sucesso.'
    else
      render :edit
    end
  end

  # DELETE /elections/1
  def destroy
    @election.destroy
    redirect_to elections_url, notice: 'Election foi apagado com sucesso.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_election
      @election = Election.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def election_params
      params.require(:election).permit(:titulo, :descricao, :inicio, :fim, election_options_attributes:[:id, :election_id, :descricao, :imagem, :nome, :numero, :_destroy])
    end
end
