class SettingsController < ApplicationController
  before_action :authenticate_user!, except: [:success]
  load_and_authorize_resource except: [:success]

  before_action :set_setting, only: [:show, :edit, :update, :destroy]

  # GET /settings
  def index
    add_breadcrumb "Configurações", settings_path
    @q = Setting.all.ransack(params[:q])
    @settings = @q.result.page(params[:page])
  end

  def atualiza_settings
    params.reject{|a,b| ["action","commit","controller","redirect","logo"].include? a }.each do |p|

      s = Setting.find_or_initialize_by(namespace: p[0])
      s.value = {"value": p[1] == "1" ? true : p[1] == "0" ? false : p[1]}

      s.save
    end

    redirect_to params[:redirect], notice: "Atualizado com sucesso!"
  end

  # GET /settings/1
  def show
  end

  # GET /settings/new
  def new
    @setting = Setting.new
  end

  # GET /settings/1/edit
  def edit
  end

  # POST /settings
  def create
    @setting = Setting.new(setting_params)

    if @setting.save
      redirect_to @setting, notice: 'Setting foi criado com sucesso'
    else
      render :new
    end
  end

  # PATCH/PUT /settings/1
  def update
    if @setting.update(setting_params)
      redirect_to @setting, notice: 'Setting foi atualizado com sucesso.'
    else
      render :edit
    end
  end

  # DELETE /settings/1
  def destroy
    @setting.destroy
    redirect_to settings_url, notice: 'Setting foi apagado com sucesso.'
  end

  def success
    render layout: "devise"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_setting
      @setting = Setting.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def setting_params
      params.require(:setting).permit(:namespace, :value)
    end
end
