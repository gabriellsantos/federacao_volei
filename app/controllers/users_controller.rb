class UsersController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  def index
    add_breadcrumb "Usuários", users_path
    @q = User.all.ransack(params[:q])
    @users = @q.result.page(params[:page])
  end

  # GET /users/1
  def show
    add_breadcrumb "Usuários", users_path
    add_breadcrumb @user.nome, user_path(@user)
  end

  # GET /users/new
  def new
    add_breadcrumb "Usuários", users_path
    add_breadcrumb "Cadastrar", new_user_path
    @user = User.new
  end

  # GET /users/1/edit
  def edit
    add_breadcrumb "Usuários", users_path
    add_breadcrumb "Alterar", edit_user_path(@user)
  end

  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to @user, notice: 'User foi criado com sucesso'
    else
      render :new
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      redirect_to @user, notice: 'User foi atualizado com sucesso.'
    else
      render :edit
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
    redirect_to users_url, notice: 'User foi apagado com sucesso.'
  end

  def home
    if current_user.admin
      redirect_to elections_path
    else
      @user = current_user
      render :show
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:cep, :cidade, :cpf, :email, :endereco, :foto_cpf_frente, :foto_cpf_frente_cache, :foto_cpf_verso, :foto_cpf_verso_cache, :foto_rg_frente, :foto_rg_frente_cache, :foto_rg_verso, :foto_rg_verso_cache, :foto_rosto, :foto_rosto_cache, :modalidade, :nascimento, :nome, :nome_pai, :nome_mae, :rg, :sexo,:telefone, :whatsapp, :ativo, :pode_votar, :admin, :cod_fed_estadual, :cod_fed_nacional)
    end
end
