# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  admin                  :boolean
#  ativo                  :boolean
#  cep                    :string
#  cidade                 :string
#  cod_fed_estadual       :string
#  cod_fed_nacional       :string
#  cpf                    :string
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  endereco               :string
#  foto_cpf_frente        :string
#  foto_cpf_verso         :string
#  foto_rg_frente         :string
#  foto_rg_verso          :string
#  foto_rosto             :string
#  modalidade             :string
#  nascimento             :date
#  nome                   :string
#  nome_mae               :string
#  nome_pai               :string
#  pode_votar             :boolean
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  rg                     :string
#  sexo                   :string
#  telefone               :string
#  whatsapp               :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#
require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
