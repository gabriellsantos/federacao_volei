require "application_system_test_case"

class ElectionOptionsTest < ApplicationSystemTestCase
  setup do
    @election_option = election_options(:one)
  end

  test "visiting the index" do
    visit election_options_url
    assert_selector "h1", text: "Election Options"
  end

  test "creating a Election option" do
    visit election_options_url
    click_on "New Election Option"

    fill_in "Descricao", with: @election_option.descricao
    fill_in "Election", with: @election_option.election_id
    fill_in "Imagem", with: @election_option.imagem
    fill_in "Nome", with: @election_option.nome
    fill_in "Numero", with: @election_option.numero
    click_on "Create Election option"

    assert_text "Election option was successfully created"
    click_on "Back"
  end

  test "updating a Election option" do
    visit election_options_url
    click_on "Edit", match: :first

    fill_in "Descricao", with: @election_option.descricao
    fill_in "Election", with: @election_option.election_id
    fill_in "Imagem", with: @election_option.imagem
    fill_in "Nome", with: @election_option.nome
    fill_in "Numero", with: @election_option.numero
    click_on "Update Election option"

    assert_text "Election option was successfully updated"
    click_on "Back"
  end

  test "destroying a Election option" do
    visit election_options_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Election option was successfully destroyed"
  end
end
