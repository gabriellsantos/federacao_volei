require 'test_helper'

class ElectionOptionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @election_option = election_options(:one)
  end

  test "should get index" do
    get election_options_url
    assert_response :success
  end

  test "should get new" do
    get new_election_option_url
    assert_response :success
  end

  test "should create election_option" do
    assert_difference('ElectionOption.count') do
      post election_options_url, params: { election_option: { descricao: @election_option.descricao, election_id: @election_option.election_id, imagem: @election_option.imagem, nome: @election_option.nome, numero: @election_option.numero } }
    end

    assert_redirected_to election_option_url(ElectionOption.last)
  end

  test "should show election_option" do
    get election_option_url(@election_option)
    assert_response :success
  end

  test "should get edit" do
    get edit_election_option_url(@election_option)
    assert_response :success
  end

  test "should update election_option" do
    patch election_option_url(@election_option), params: { election_option: { descricao: @election_option.descricao, election_id: @election_option.election_id, imagem: @election_option.imagem, nome: @election_option.nome, numero: @election_option.numero } }
    assert_redirected_to election_option_url(@election_option)
  end

  test "should destroy election_option" do
    assert_difference('ElectionOption.count', -1) do
      delete election_option_url(@election_option)
    end

    assert_redirected_to election_options_url
  end
end
