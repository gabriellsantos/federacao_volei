Rails.application.routes.draw do
  resources :election_options
  resources :elections do
    member do
      get "apuracao"
    end
    collection do
      get "votar"
      get "recaptcha"
    end
  end
  resources :settings do
    collection do
      post "atualiza_settings"
      get "success"
    end
  end

  devise_for :users, controllers: { registrations: "registrations" }

  resources :users

  root  "users#home"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
