class CreateElections < ActiveRecord::Migration[6.0]
  def change
    create_table :elections do |t|
      t.string :titulo
      t.text :descricao
      t.datetime :inicio
      t.datetime :fim

      t.timestamps
    end
  end
end
