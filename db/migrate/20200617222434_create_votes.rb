class CreateVotes < ActiveRecord::Migration[6.0]
  def change
    create_table :votes do |t|
      t.references :election_option, null: false, foreign_key: true
      t.string :chave
    end
  end
end
