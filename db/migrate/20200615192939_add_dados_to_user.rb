class AddDadosToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :modalidade, :string
    add_column :users, :nome, :string
    add_column :users, :sexo, :string
    add_column :users, :nascimento, :date
    add_column :users, :cpf, :string
    add_column :users, :rg, :string
    add_column :users, :endereco, :string
    add_column :users, :cidade, :string
    add_column :users, :cep, :string
    add_column :users, :telefone, :string
    add_column :users, :whatsapp, :string
    add_column :users, :nome_pai, :string
    add_column :users, :nome_mae, :string
    add_column :users, :foto_rosto, :string
    add_column :users, :foto_rg_frente, :string
    add_column :users, :foto_rg_verso, :string
    add_column :users, :foto_cpf_frente, :string
    add_column :users, :foto_cpf_verso, :string
    add_column :users, :ativo, :boolean
    add_column :users, :cod_fed_estadual, :string
    add_column :users, :cod_fed_nacional, :string
    add_column :users, :pode_votar, :boolean
  end
end
