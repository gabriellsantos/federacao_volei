class CreateMemberVotes < ActiveRecord::Migration[6.0]
  def change
    create_table :member_votes do |t|
      t.references :election, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.string :chave
    end
  end
end
