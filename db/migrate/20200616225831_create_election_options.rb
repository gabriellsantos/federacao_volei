class CreateElectionOptions < ActiveRecord::Migration[6.0]
  def change
    create_table :election_options do |t|
      t.references :election, null: false, foreign_key: true
      t.string :nome
      t.text :descricao
      t.string :numero
      t.string :imagem

      t.timestamps
    end
  end
end
