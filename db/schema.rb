# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_17_222544) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "election_options", force: :cascade do |t|
    t.bigint "election_id", null: false
    t.string "nome"
    t.text "descricao"
    t.string "numero"
    t.string "imagem"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["election_id"], name: "index_election_options_on_election_id"
  end

  create_table "elections", force: :cascade do |t|
    t.string "titulo"
    t.text "descricao"
    t.datetime "inicio"
    t.datetime "fim"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "member_votes", force: :cascade do |t|
    t.bigint "election_id", null: false
    t.bigint "user_id", null: false
    t.string "chave"
    t.index ["election_id"], name: "index_member_votes_on_election_id"
    t.index ["user_id"], name: "index_member_votes_on_user_id"
  end

  create_table "settings", force: :cascade do |t|
    t.string "namespace"
    t.json "value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "modalidade"
    t.string "nome"
    t.string "sexo"
    t.date "nascimento"
    t.string "cpf"
    t.string "rg"
    t.string "endereco"
    t.string "cidade"
    t.string "cep"
    t.string "telefone"
    t.string "whatsapp"
    t.string "nome_pai"
    t.string "nome_mae"
    t.string "foto_rosto"
    t.string "foto_rg_frente"
    t.string "foto_rg_verso"
    t.string "foto_cpf_frente"
    t.string "foto_cpf_verso"
    t.boolean "ativo"
    t.string "cod_fed_estadual"
    t.string "cod_fed_nacional"
    t.boolean "pode_votar"
    t.boolean "admin"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "votes", force: :cascade do |t|
    t.bigint "election_option_id", null: false
    t.string "chave"
    t.index ["election_option_id"], name: "index_votes_on_election_option_id"
  end

  add_foreign_key "election_options", "elections"
  add_foreign_key "member_votes", "elections"
  add_foreign_key "member_votes", "users"
  add_foreign_key "votes", "election_options"
end
